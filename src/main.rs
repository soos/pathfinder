extern crate ncollide;
extern crate nalgebra;
extern crate svg;

use nalgebra::{Point2, Vector1, Vector2, Isometry2, Norm};

use ncollide::shape::{Shape, Cuboid};
use ncollide::query::{Ray, RayCast};
use ncollide::bounding_volume::{BoundingVolume, AABB};

struct Obstacle {
    shape: Cuboid<Vector2<f32>>, //TODO Shape
    xy: Vector2<f32>, //TODO Transform
}

impl Obstacle {
    fn new (center: Vector2<f32>, half_extends: Vector2<f32>) -> Obstacle {
        Obstacle {
            shape: Cuboid::new(half_extends),
            xy: center,
        }
    }

    fn aabb (&self) -> AABB<Point2<f32>> {
        self.shape.aabb(&Isometry2::new(self.xy, Vector1::new(0.)))
    }

    fn vertices (&self, margin: f32) -> (Point2<f32>, Point2<f32>, Point2<f32>, Point2<f32>) {
        let aabb = self.aabb().loosened(margin);
        let min = aabb.mins();
        let max = aabb.maxs();
        (*min, Point2::new(max.x, min.y), *max, Point2::new(min.x, max.y))
    }
}

struct Segment {
    p1: Point2<f32>, //TODO Point<Scalar>
    p2: Point2<f32>, //TODO Point<Scalar>
}

impl Segment {
    fn new (p1: Point2<f32>, p2: Point2<f32>) -> Segment {
        Segment {
            p1: p1,
            p2: p2,
        }
    }

    fn intersects (&self, obs: &Obstacle) -> bool {
        let ray1 = Ray::new(self.p1, self.p2 - self.p1);
        let ray2 = Ray::new(self.p2, self.p1 - self.p2);

        obs.shape.intersects_ray(&obs.xy, &ray1) &&
        obs.shape.intersects_ray(&obs.xy, &ray2)
    }

    fn intersects_any (&self, obs: &[Obstacle]) -> bool {
        for ob in obs {
            if self.intersects(ob) { return true; }
        }
        false
    }
}

#[derive(Clone,Copy,Debug)]
struct Edge {
    a: usize,
    b: usize,
    len: f32,
}

impl Edge {
    fn new (a: usize, b: usize, len: f32) -> Edge {
        Edge {
            a: a,
            b: b,
            len: len,
        }
    }
}

struct Graph {
    vertices: Vec<Point2<f32>>,
    edges: Vec<Edge>,
}

impl Graph {
    fn new () -> Graph {
        Graph {
            vertices: Vec::new(),
            edges: Vec::new(),
        }
    }

    fn add_vertex (&mut self, vertex: Point2<f32>) -> usize {
        self.vertices.push(vertex);
        self.vertices.len() - 1
    }

    fn add_edge (&mut self, a: usize, b: usize) {
        self.edges.push(Edge::new(a,b,(self.vertices[a]-self.vertices[b]).norm()));
    }

    fn shortest_path (&self, i1: usize, i2: usize) -> Vec<Point2<f32>> {
        #[derive(Clone)]
        struct State {
            dist: Option<f32>,
            prev: Option<usize>,
            out: bool,
        }
        impl State {
            fn new () -> State {
                State {
                    dist: None,
                    prev: None,
                    out: false,
                }
            }
        }
        fn find_edge (edges: &[Edge], a: usize, b: usize) -> Option<Edge> {
            for &edge in edges {
                if (edge.a == a && edge.b == b) || (edge.a == b && edge.b == a) { return Some(edge); }
            }
            None
        }
        //println!("search for: {} -> {}", i1, i2);
        let mut states = vec![State::new(); self.vertices.len()];
        states[i1].dist = Some(0.);
        loop {
            let mut u: Option<usize> = None;
            for i in 0..states.len() {
                if ! states[i].out {
                    if let Some(dist) = states[i].dist {
                        match u {
                            Some(prev_i) => {
                                if let Some(prev_dist) = states[prev_i].dist {
                                    if dist < prev_dist { u = Some(i); }
                                }
                            }
                            None => { u = Some(i); }
                        }
                    }
                }
            }
            match u {
                Some(u) => {
                    //println!("found: u = {}", u);
                    states[u].out = true;
                    //XXX maybe iterate over edges is faster ???
                    for v in 0..states.len() {
                        if states[v].out { continue; }
                        let edge = find_edge(&self.edges, u, v);
                        if let Some(edge) = edge {
                            //println!("neighbor {} len {}", v, edge.len);
                            let alt = match states[u].dist {
                                Some(dist) => dist + edge.len,
                                None => edge.len,
                            };
                            match states[v].dist {
                                Some(dist) => {
                                    if alt < dist {
                                        //println!("neighbor {} set new dist {}", v, alt);
                                        states[v].dist = Some(alt);
                                        states[v].prev = Some(u);
                                    }
                                }
                                None => {
                                    //println!("neighbor {} set dist {}", v, alt);
                                    states[v].dist = Some(alt);
                                    states[v].prev = Some(u);
                                }
                            }
                        }
                    }
                }
                None => break,
            }
        }
        let mut path = Vec::new();
        let mut idx = i2;
        while let Some(prev) = states[idx].prev {
            path.push(self.vertices[idx]);
            idx = prev;
        }
        path.push(self.vertices[i1]);
        //TODO path.invert()
        path
    }
}

struct VisibilityGraph {
    graph: Graph,
    source_index: usize,
    target_index: usize,
}

impl VisibilityGraph {
    fn build (obs: &[Obstacle], p1: Point2<f32>, p2: Point2<f32>, margin: f32, half_size: f32) -> VisibilityGraph {

        let obs: Vec<Obstacle> = obs.iter().map(|ob| {Obstacle::new(ob.xy, *ob.shape.half_extents() + Vector2::new(half_size, half_size) )}).collect();

        let mut graph = Graph::new();

        let p1i = graph.add_vertex(p1);
        let p2i = graph.add_vertex(p2);

        let mut graph_indexes = Vec::new();

        for ob in obs.iter() {
            let (v1,v2,v3,v4) = ob.vertices(margin);

            let v1i = graph.add_vertex(v1);
            let v2i = graph.add_vertex(v2);
            let v3i = graph.add_vertex(v3);
            let v4i = graph.add_vertex(v4);

            graph.add_edge(v1i,v2i);
            graph.add_edge(v2i,v3i);
            graph.add_edge(v3i,v4i);
            graph.add_edge(v4i,v1i);

            if !Segment::new(p1,v1).intersects_any(&obs) { graph.add_edge(p1i,v1i); }
            if !Segment::new(p1,v2).intersects_any(&obs) { graph.add_edge(p1i,v2i); }
            if !Segment::new(p1,v3).intersects_any(&obs) { graph.add_edge(p1i,v3i); }
            if !Segment::new(p1,v4).intersects_any(&obs) { graph.add_edge(p1i,v4i); }

            if !Segment::new(p2,v1).intersects_any(&obs) { graph.add_edge(p2i,v1i); }
            if !Segment::new(p2,v2).intersects_any(&obs) { graph.add_edge(p2i,v2i); }
            if !Segment::new(p2,v3).intersects_any(&obs) { graph.add_edge(p2i,v3i); }
            if !Segment::new(p2,v4).intersects_any(&obs) { graph.add_edge(p2i,v4i); }

            graph_indexes.push((v1i,v2i,v3i,v4i));
        }

        //TODO remove disconnected vertices

        for i in 0..graph_indexes.len() {
            for j in i+1..graph_indexes.len() {
                //println!("{} -> {}", i, j);

                let a = graph_indexes[i];
                let b = graph_indexes[j];

                if !Segment::new(graph.vertices[a.0], graph.vertices[b.0]).intersects_any(&obs) { graph.add_edge(a.0, b.0); }
                if !Segment::new(graph.vertices[a.0], graph.vertices[b.1]).intersects_any(&obs) { graph.add_edge(a.0, b.1); }
                if !Segment::new(graph.vertices[a.0], graph.vertices[b.2]).intersects_any(&obs) { graph.add_edge(a.0, b.2); }
                if !Segment::new(graph.vertices[a.0], graph.vertices[b.3]).intersects_any(&obs) { graph.add_edge(a.0, b.3); }

                if !Segment::new(graph.vertices[a.1], graph.vertices[b.0]).intersects_any(&obs) { graph.add_edge(a.1, b.0); }
                if !Segment::new(graph.vertices[a.1], graph.vertices[b.1]).intersects_any(&obs) { graph.add_edge(a.1, b.1); }
                if !Segment::new(graph.vertices[a.1], graph.vertices[b.2]).intersects_any(&obs) { graph.add_edge(a.1, b.2); }
                if !Segment::new(graph.vertices[a.1], graph.vertices[b.3]).intersects_any(&obs) { graph.add_edge(a.1, b.3); }

                if !Segment::new(graph.vertices[a.2], graph.vertices[b.0]).intersects_any(&obs) { graph.add_edge(a.2, b.0); }
                if !Segment::new(graph.vertices[a.2], graph.vertices[b.1]).intersects_any(&obs) { graph.add_edge(a.2, b.1); }
                if !Segment::new(graph.vertices[a.2], graph.vertices[b.2]).intersects_any(&obs) { graph.add_edge(a.2, b.2); }
                if !Segment::new(graph.vertices[a.2], graph.vertices[b.3]).intersects_any(&obs) { graph.add_edge(a.2, b.3); }

                if !Segment::new(graph.vertices[a.3], graph.vertices[b.0]).intersects_any(&obs) { graph.add_edge(a.3, b.0); }
                if !Segment::new(graph.vertices[a.3], graph.vertices[b.1]).intersects_any(&obs) { graph.add_edge(a.3, b.1); }
                if !Segment::new(graph.vertices[a.3], graph.vertices[b.2]).intersects_any(&obs) { graph.add_edge(a.3, b.2); }
                if !Segment::new(graph.vertices[a.3], graph.vertices[b.3]).intersects_any(&obs) { graph.add_edge(a.3, b.3); }
            }
        }

        VisibilityGraph {
            graph: graph,
            source_index: p1i,
            target_index: p2i,
        }
    }

    fn shortest_path (&self) -> Vec<Point2<f32>> {
        self.graph.shortest_path(self.source_index, self.target_index)
    }
}

fn main() {
    use svg::Document;
    use svg::node::element::{Line, Text, Rectangle};

    let source = Point2::new(0.,0.);
    let target = Point2::new(60.,60.);
    let half_size = 2.0;
    let margin = 0.05;

    let obstacles = [
        Obstacle::new(Vector2::new(20., 15.), Vector2::new(10., 5.)),
        Obstacle::new(Vector2::new(30., 40.), Vector2::new(5., 10.)),
        Obstacle::new(Vector2::new(55., 50.), Vector2::new(5., 5.)),
    ];

    let graph = VisibilityGraph::build(&obstacles, source, target, margin, half_size);
    let path = graph.shortest_path();

    /*
    for edge in &graph.edges {
        println!("{:?}", edge);
    }
    */

    let mut document = Document::new();
    
    for ob in obstacles.iter() {
        let aabb = ob.aabb();
        let mins = aabb.mins();
        let maxs = aabb.maxs();
        let rect = Rectangle::new()
            .set("fill", "gray")
            .set("stroke", "none")
            //.set("stroke-width", 0)
            .set("x", mins.x)
            .set("y", mins.y)
            .set("width", maxs.x - mins.x)
            .set("height", maxs.y - mins.y);
        document = document.add(rect);
    }

    for edge in graph.graph.edges.iter() {
        let line = Line::new().set("stroke", "black").set("stroke-width", 0.1)
            .set("x1", graph.graph.vertices[edge.a].x)
            .set("y1", graph.graph.vertices[edge.a].y)
            .set("x2", graph.graph.vertices[edge.b].x)
            .set("y2", graph.graph.vertices[edge.b].y);
        document = document.add(line);
    }

    for (i,vertex) in graph.graph.vertices.iter().enumerate() {
        let text = Text::new()
            .set("font-family", "Verdana")
            .set("font-size", "3")
            .set("fill","blue")
            .set("x", vertex.x)
            .set("y", vertex.y)
            .add(svg::node::Text::new(format!("{}", i)));
        document = document.add(text);
    }

    //println!("{:?}", path);
    let mut iter = path.iter();
    iter.next();
    let pairs = path.iter().zip(iter).collect::<Vec<(&Point2<f32>,&Point2<f32>)>>();
    for (a,b) in pairs {
        let line = Line::new().set("stroke", "red").set("stroke-width", 0.1)
            .set("x1", a.x)
            .set("y1", a.y)
            .set("x2", b.x)
            .set("y2", b.y);
        document = document.add(line);
    }

    svg::save("image.svg", &document).unwrap();
}
